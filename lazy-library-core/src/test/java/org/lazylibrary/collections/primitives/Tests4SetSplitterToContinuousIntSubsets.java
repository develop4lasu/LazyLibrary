/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.collections.primitives;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Test;
import org.lazylibrary.lazy.LazyPrimitives;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public final class Tests4SetSplitterToContinuousIntSubsets {

	//TODO check unique

	@Test
	public final void ensureTestName() {
		assertEquals("Tests4" + SetSplitterToContinuousIntSubsets.class.getSimpleName(), this.getClass().getSimpleName());
	}

	@Test
	public final void ensureNextSubSetsDiffer() {
		for (int size = 0; size < 8; size++) {
			final SetSplitterToContinuousIntSubsets setSplitter = new SetSplitterToContinuousIntSubsets(size);
			int[] pre = null;
			for (int[] is : setSplitter) {
				if (Arrays.equals(pre, is)) {
					fail("To elements are equals (" + LazyPrimitives.compaction.toString(pre) + " == " + LazyPrimitives.compaction.toString(is) + ")");
				}
			}
		}
	}

	@Test
	public final void ensureSortedSplits() {
		for (int size = 0; size < 8; size++) {
			final SetSplitterToContinuousIntSubsets setSplitter = new SetSplitterToContinuousIntSubsets(size);
			int[] pre = null;
			for (int[] is : setSplitter) {
				ensureSortedSplits(is);
				if (Arrays.equals(pre, is)) {
					fail("To elements are equals (" + LazyPrimitives.compaction.toString(pre) + " == " + LazyPrimitives.compaction.toString(is) + ")");
				}
			}
		}
	}

	public final void ensureSortedSplits(int[] subSets) {
		short preUpper = (short) -1;
		for (final int compact : subSets) {
			final short lower = LazyPrimitives.compaction.getLower(compact);
			final short upper = LazyPrimitives.compaction.getUpper(compact);
			assertTrue("lower(" + lower + ")" + "<=" + "upper(" + upper + ")", lower <= upper);
			assertEquals("Is continuous", preUpper + 1, lower);
			preUpper = upper;
		}
	}

}
