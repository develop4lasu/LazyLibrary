/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.collections.primitives;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * @see IntRangeSet
 */
public class Tests4ContinuousIntIndexCollection {
	@Test
	public final void ensureAddingAndRemoving() {
		for (int[] range : new int[][] { new int[] { -1, 1 }, new int[] { 0, 1 }, new int[] { 111, 112 }, new int[] { -1, 12 }, new int[] { -1, 122222 } }) {
			final int start = range[0];
			final int end = range[1];

			final IntRangeSet indexes = new IntRangeSet(start, end);
			for /* adding */ (int i = start; i < end; i++) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
			}
			for /* removing */ (int i = start; i < end; i++) {
				assertTrue(indexes.contains(i));
				indexes.remove(i);
				assertFalse(indexes.contains(i));
			}
			for/* separation */ (int i = start; i < end; i += 2) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
				for (int n = start + 1; n < end; n += 2) {
					assertFalse(indexes.contains(n));
				}
			}
		}
	}

	@Test
	public final void ensureAddingAndRemovingCh() {
		for (int[] range : new int[][] { new int[] { 0, 1 }, new int[] { 0, 1 }, new int[] { 111, 112 }, new int[] { 0, 12 }, new int[] { 0, 122222 } }) {
			final int start = range[0];
			final int end = range[1];

			final IntRangeSetAlternative04 indexes = new IntRangeSetAlternative04(end);
			for /* adding */ (int i = start; i < end; i++) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
			}
			for /* removing */ (int i = start; i < end; i++) {
				assertTrue(indexes.contains(i));
				indexes.remove(i);
				assertFalse(indexes.contains(i));
			}
			for/* separation */ (int i = start; i < end; i += 2) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
				for (int n = start + 1; n < end; n += 2) {
					assertFalse(indexes.contains(n));
				}
			}
		}
	}

	@Test
	public final void ensureAddingAndRemoving40() {
		for (int[] range : new int[][] { new int[] { 0, 1 }, new int[] { 0, 1 }, new int[] { 111, 112 }, new int[] { 1, 12 }, new int[] { 2, 122222 } }) {
			final int start = range[0];
			final int end = range[1];

			final IntRangeSetAlternative01 indexes = new IntRangeSetAlternative01(end);
			for /* adding */ (int i = start; i < end; i++) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
			}
			for /* removing */ (int i = start; i < end; i++) {
				assertTrue(indexes.contains(i));
				indexes.remove(i);
				assertFalse(indexes.contains(i));
			}
			for/* separation */ (int i = start; i < end; i += 2) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
				for (int n = start + 1; n < end; n += 2) {
					assertFalse(indexes.contains(n));
				}
			}
		}
	}

	@Test
	public final void ensureAddingAndRemoving4B() {
		for (int[] range : new int[][] { new int[] { -1, 1 }, new int[] { 0, 1 }, new int[] { 111, 112 }, new int[] { -1, 12 }, new int[] { -1, 122222 } }) {
			final int start = range[0];
			final int end = range[1];

			final IntRangeSetAlternative02 indexes = new IntRangeSetAlternative02(start, end);
			for /* adding */ (int i = start; i < end; i++) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
			}
			for /* removing */ (int i = start; i < end; i++) {
				assertTrue(indexes.contains(i));
				indexes.remove(i);
				assertFalse(indexes.contains(i));
			}
			for/* separation */ (int i = start; i < end; i += 2) {
				assertFalse(indexes.contains(i));
				indexes.add(i);
				assertTrue(indexes.contains(i));
				for (int n = start + 1; n < end; n += 2) {
					assertFalse(indexes.contains(n));
				}
			}
		}
	}

	@Test
	public final void ensureCopy() {
		for (int[] range : new int[][] { new int[] { -1, 1 }, new int[] { 0, 1 }, new int[] { 111, 112 }, new int[] { -1, 12 }, new int[] { -1, 122222 } }) {
			final int start = range[0];
			final int end = range[1];

			final boolean[] indexes = new boolean[end - start];
			for /* adding */ (int i = start; i < end; i++) {
				assertFalse(indexes[i - start]);
				indexes[i - start] = true;
				assertTrue(indexes[i - start]);
			}
			for /* removing */ (int i = start; i < end; i++) {
				assertTrue(indexes[i - start]);
				indexes[i - start] = false;
				assertFalse(indexes[i - start]);
			}
			for/* separation */ (int i = start; i < end; i += 2) {
				assertFalse(indexes[i - start]);
				indexes[i - start] = true;
				assertTrue(indexes[i - start]);
				for (int n = start + 1; n < end; n += 2) {
					assertFalse(indexes[n - start]);
				}
			}
		}
	}
}
