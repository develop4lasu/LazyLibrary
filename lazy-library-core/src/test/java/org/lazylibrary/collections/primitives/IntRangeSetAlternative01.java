/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.collections.primitives;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class IntRangeSetAlternative01 {
	final static boolean finalValidation = true;

	private static int[] posToMask;
	static {
		posToMask = new int[32];
		for (int i = 0, m = 1; i < posToMask.length; i++) {
			posToMask[i] = m;
			m <<= 1;
		}
	}
	final int[] byteMarks;
	final int proposedStart = 0;

	final int proposedStop;

	public IntRangeSetAlternative01(int proposedStop) {
		this.proposedStop = proposedStop;
		byteMarks = new int[(proposedStop >> 5) + 1];
	}

	public IntRangeSetAlternative01 add(int element) {
		final int index = (element >> 5) - 0;
		if (index <= 0) {
			if (element < proposedStart) { throw new IllegalArgumentException(
					"Element(" + element + ") is outside of range <" + proposedStart + ";" + proposedStop + ">"); }
		}
		else if (index >= byteMarks.length) {
			if (element > proposedStop) { throw new IllegalArgumentException(
					"Element(" + element + ") is outside of range <" + proposedStart + ";" + proposedStop + ">"); }
		}
		byteMarks[index] |= (1 << (element & 0b11111));
		return this;
	}

	public boolean contains(int element) {

		final int index = (element >> 5);
		if (index <= 0) {
			if (element < proposedStart) { throw new IllegalArgumentException(
					"Element(" + element + ") is outside of range <" + proposedStart + ";" + proposedStop + ">"); }
		}
		else if (index >= byteMarks.length) {
			if (element > proposedStop) { throw new IllegalArgumentException(
					"Element(" + element + ") is outside of range <" + proposedStart + ";" + proposedStop + ">"); }
		}
		final int group = byteMarks[index];

		final int bytePos = element & 0b11111;
		return (group & (1 << bytePos)) != 0;

	}

	public int getMaximalSupportedValue() {
		return proposedStop;
	}

	public int getMinimalSupportedValue() {
		return proposedStart;
	}

	public IntRangeSetAlternative01 remove(int element) {
		final int index = (element >> 5) - 0;
		if (index <= 0) {
			if (element < proposedStart) { throw new IllegalArgumentException(
					"Element(" + element + ") is outside of range <" + proposedStart + ";" + proposedStop + ">"); }
		}
		else if (index >= byteMarks.length) {
			if (element > proposedStop) { throw new IllegalArgumentException(
					"Element(" + element + ") is outside of range <" + proposedStart + ";" + proposedStop + ">"); }
		}
		byteMarks[index] &= ~(1 << (element & 0b11111));
		return this;
	}

}
