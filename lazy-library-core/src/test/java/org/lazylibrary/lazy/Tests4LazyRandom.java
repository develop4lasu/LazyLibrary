/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.lazylibrary.traits.IsFalseNegativePossible;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class Tests4LazyRandom {

	@Test
	@IsFalseNegativePossible(probablity = "3/2^110")
	public void checkForToWideRange() {
		for (int range : new int[] { 111, 1111, 11111 }) {
			final int[] ints = LazyRandom.intsInRange(1, range * 2 + 1, 111);
			boolean outside = false;
			for (int r : ints) {
				if (r < -range || r > range) {
					outside = true;
					break;
				}
			}
			Assert.assertTrue("There are no elements outside of scope", outside);
		}
	}

	@Test
	@IsFalseNegativePossible(probablity = "5/2^110")
	public void ensureIntsInRange() {
		for /* single */ (final int single : new int[] { Integer.MIN_VALUE, 0, 1, -1, Integer.MAX_VALUE - 1 }) {
			final int[] ints = LazyRandom.intsInRange(single, single + 1, 11);
			for (final int a : ints) {
				Assert.assertEquals(single, a);
			}
		}
		for /* single */ (final int theDouble : new int[] { Integer.MIN_VALUE, 0, 1, -1, Integer.MAX_VALUE - 2 }) {
			final int[] ints = LazyRandom.intsInRange(theDouble, theDouble + 2, 111);
			int counts0 = 0;
			int counts1 = 0;
			for (final int a : ints) {
				if (a == theDouble) {
					counts0++;
				}
				else if (a == theDouble + 1) {
					counts1++;
				}
				else {
					Assert.fail("Invalud element(" + a + ") found ");//FIXME desc
				}
			}
			Assert.assertTrue(Arrays.toString(ints) + " do not contains element(" + (theDouble) + ")", counts0 > 0);
			Assert.assertTrue(Arrays.toString(ints) + " do not contains element(" + (theDouble + 1) + ")", counts1 > 0);
		}
	}

	@Test
	@IsFalseNegativePossible(probablity = "low")
	public void ensureIntsInRangeForWideRange() {
		for (int range : new int[] { 111, 1111, 11111 }) {
			final int[] ints = LazyRandom.intsInRange(-range, range + 1, 111);
			final HashSet<Integer> elements = new HashSet<Integer>();
			for (int r : ints) {
				if (r < -range || r > range) {
					Assert.fail(//
							"Randome element(" + r + ") outside of range " + LazyArray.clasicRangeToDesc(-range, range + 1));
				}
				elements.add(r);
			}
			if (elements.size() < 22) {
				Assert.fail(Random.class.getSimpleName() + " failed " + Arrays.toString(ints));
			}
		}
	}

}
