/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Random;

import org.junit.Test;
import org.lazylibrary.lazy.LazyPrimitives.cast;
import org.lazylibrary.lazy.LazyPrimitives.compaction;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class Tests4LazyPrimitives {

	private static void assertLowerChange(byte value, byte lower) {
		final short compact = compaction.compact(value, value);
		assertEquals(value, compaction.getLower(compact));
		assertEquals(value, compaction.getUpper(compact));

		final short changed = compaction.putLower(compact, lower);
		assertEquals("lower for: " + compact + ".putLower(" + lower + ") => " + changed, lower, compaction.getLower(changed));
		assertEquals("upper for: " + compact + ".putLower(" + lower + ") => " + changed, value, compaction.getUpper(changed));

		final short changedBack = compaction.putLower(compact, value);
		assertEquals("value(" + value + ") & lower(" + lower + ")", compact, changedBack);
	}

	private static void assertLowerChange(int value, int lower) {
		final long compact = compaction.compact(value, value);
		assertEquals(value, compaction.getLower(compact));
		assertEquals(value, compaction.getUpper(compact));

		final long changed = compaction.putLower(compact, lower);
		assertEquals("lower for: " + compact + ".putLower(" + lower + ") => " + changed, lower, compaction.getLower(changed));
		assertEquals("upper for: " + compact + ".putLower(" + lower + ") => " + changed, value, compaction.getUpper(changed));

		final long changedBack = compaction.putLower(compact, value);
		assertEquals("value(" + value + ") & lower(" + lower + ")", compact, changedBack);
	}

	private static void assertLowerChange(short value, short lower) {
		final int compact = compaction.compact(value, value);
		assertEquals(value, compaction.getLower(compact));
		assertEquals(value, compaction.getUpper(compact));

		final int changed = compaction.putLower(compact, lower);
		assertEquals("lower for: " + compact + ".putLower(" + lower + ") => " + changed, lower, compaction.getLower(changed));
		assertEquals("upper for: " + compact + ".putLower(" + lower + ") => " + changed, value, compaction.getUpper(changed));

		final int changedBack = compaction.putLower(compact, value);
		assertEquals("value(" + value + ") & lower(" + lower + ")", compact, changedBack);
	}

	private static void assertUpperChange(byte value, byte upper) {
		final short compact = compaction.compact(value, value);
		assertEquals(value, compaction.getLower(compact));
		assertEquals(value, compaction.getUpper(compact));

		final short changed = compaction.putUpper(compact, upper);
		assertEquals("lower for: " + compact + ".putUpper(" + upper + ") => " + changed, value, compaction.getLower(changed));
		assertEquals("upper for: " + compact + ".putUpper(" + upper + ") => " + changed, upper, compaction.getUpper(changed));

		final short changedBack = compaction.putUpper(compact, value);
		assertEquals("value(" + value + ") & upper(" + upper + ")", compact, changedBack);
	}

	private static void assertUpperChange(int value, int upper) {
		final long compact = compaction.compact(value, value);
		assertEquals(value, compaction.getLower(compact));
		assertEquals(value, compaction.getUpper(compact));

		final long changed = compaction.putUpper(compact, upper);
		assertEquals("lower for: " + compact + ".putUpper(" + upper + ") => " + changed, value, compaction.getLower(changed));
		assertEquals("upper for: " + compact + ".putUpper(" + upper + ") => " + changed, upper, compaction.getUpper(changed));

		final long changedBack = compaction.putUpper(compact, value);
		assertEquals("value(" + value + ") & upper(" + upper + ")", compact, changedBack);
	}

	private static void assertUpperChange(short value, short upper) {
		final int compact = compaction.compact(value, value);
		assertEquals(value, compaction.getLower(compact));
		assertEquals(value, compaction.getUpper(compact));

		final int changed = compaction.putUpper(compact, upper);
		assertEquals("lower for: " + compact + ".putUpper(" + upper + ") => " + changed, value, compaction.getLower(changed));
		assertEquals("upper for: " + compact + ".putUpper(" + upper + ") => " + changed, upper, compaction.getUpper(changed));

		final int changedBack = compaction.putUpper(compact, value);
		assertEquals("value(" + value + ") & upper(" + upper + ")", compact, changedBack);
	}

	private final byte[] bytes;

	private final int[] ints;

	private final short[] shorts;

	public Tests4LazyPrimitives() {
		final Random random = new Random(System.currentTimeMillis());
		{
			final ByteArrayOutputStream bytes = new ByteArrayOutputStream(250);
			bytes.write(random.nextInt());
			bytes.write(random.nextInt());
			bytes.write(random.nextInt());
			bytes.write(random.nextInt());
			bytes.write(0);
			bytes.write(~0);
			for (int b = 0b10000000; b > 0; b >>>= 1) {
				bytes.write(b);
				bytes.write(~b);
			}
			for (int b = 0b11111100; (b & 0xFF) != 0; b <<= 1) {
				bytes.write(b);
				bytes.write(~b);
			}
			this.bytes = bytes.toByteArray();
		}
		{
			final short[] ret = new short[256];
			int len = 0;
			ret[len++] = (short) random.nextInt();
			ret[len++] = (short) random.nextInt();
			ret[len++] = (short) random.nextInt();
			ret[len++] = (short) random.nextInt();
			ret[len++] = 0;
			ret[len++] = ~0;
			for (short b = 0b100000000000000; b > 0; b >>>= 1) {
				ret[len++] = b;
				ret[len++] = (short) ~b;
			}
			for (short b = 0b111111111111110; b != 0; b <<= 1) {
				ret[len++] = b;
				ret[len++] = (short) ~b;
			}
			this.shorts = Arrays.copyOf(ret, len);
		}
		{
			final int[] ret = new int[256];
			int len = 0;
			ret[len++] = random.nextInt();
			ret[len++] = random.nextInt();
			ret[len++] = random.nextInt();
			ret[len++] = random.nextInt();
			ret[len++] = 0;
			for (int b = 0x80000000; b > 0; b >>>= 1) {
				ret[len++] = b;
				ret[len++] = ~b;
			}
			for (int b = 0xFFFFFFFF; b != 0; b <<= 1) {
				ret[len++] = b;
				ret[len++] = ~b;
			}
			this.ints = Arrays.copyOf(ret, len);
		}
	}

	@Test
	public void ensureCompaction4Bytes() {
		for /* putUpper */ (final byte value : bytes)
			for (byte upper : bytes) {
				assertUpperChange(value, upper);

			}
		for (byte value : bytes)
			for (byte lower : bytes) {
				assertLowerChange(value, lower);
			}
	}

	@Test
	public void ensureCompaction4Ints() {
		for /* putUpper */ (final int value : ints)
			for (int upper : ints) {
				assertUpperChange(value, upper);

			}
		for (int value : ints)
			for (int lower : ints) {
				assertLowerChange(value, lower);
			}
	}

	@Test
	public void ensureCompaction4Shorts() {
		for /* putUpper */ (final short value : shorts)
			for (short upper : shorts) {
				assertUpperChange(value, upper);

			}
		for (short value : shorts)
			for (short lower : shorts) {
				assertLowerChange(value, lower);
			}
	}

	@Test
	public void ensure2ByteCast() {
		for (long l = Byte.MAX_VALUE; l > Byte.MIN_VALUE; l--) {
			assertEquals(l, cast.toByte(l));
			assertEquals(l, cast.toByte(cast.toShort(l)));
			assertEquals(l, cast.toByte(cast.toShort(cast.toInt(l))));
		}

	}

}
