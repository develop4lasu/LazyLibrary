/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Assert;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class Tests4LazyMath {

	@Test
	public void ensureCreateIntDecoder() {
		final int[] createIntDecoder = LazyMath.createIntDecoder();
		assertNotNull("decoder", createIntDecoder);
		assertEquals(32 /* bits */, createIntDecoder.length);
		for (int i = 0; i < createIntDecoder.length; i++) {
			int dec = createIntDecoder[i];
			assertNotEquals(0, dec);
			assertEquals(1, dec >>> i);
			assertEquals(dec, 1 << i);
		}

		// int[] 
	}

	@Test
	public void ensureCountSubSets4BitFullSet() {
		for (int s = 1; s < 32; s++) {
			Assert.assertEquals(1, LazyMath.bitFullSet.countSubSets(s, 0xFFFFFFFF));
			Assert.assertEquals(1, LazyMath.bitFullSet.countSubSets(s, 0));
			Assert.assertEquals(s, LazyMath.bitFullSet.countSubSets(s, 0b10101010101010101010101010101010));
			Assert.assertEquals((s + 1) / 2, LazyMath.bitFullSet.countSubSets(s, 0b11001100110011001100110011001100));
		}

		for (int s = 2; s < 32; s++)
			for (int j = 1; j < s - 1; j++) {
				Assert.assertEquals(2, LazyMath.bitFullSet.countSubSets(s, 0xFFFFFFFF << j));
				Assert.assertEquals(2, LazyMath.bitFullSet.countSubSets(s, ~(0xFFFFFFFF << j)));
			}
		for (int s = 3; s < 32; s++)
			for (int j = 1; j < s - 1; j++) {
				Assert.assertEquals(3, LazyMath.bitFullSet.countSubSets(s, 1 << j));
				Assert.assertEquals(3, LazyMath.bitFullSet.countSubSets(s, ~(1 << j)));
			}
	}
}
