/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * @see org.lazylibrary.lazy.LazyArray
 */
@SuppressWarnings("null")
public final class Tests4LazyArray {

	private Random random = new Random(System.currentTimeMillis());

	private void assertArrayNotEquals(Object[] expecteds, Object[] actuals) {
		try {
			assertArrayEquals(expecteds, actuals);
		}
		catch (AssertionError e) {
			return;
		}
		fail("The arrays are not equal");
	}

	@Test
	public void ensureMutableReverse() {
		assertArrayEquals(new Object[] {}, LazyArray.mutable.reverse(new Object[] {}));
		assertEquals(Object.class, LazyArray.mutable.reverse(new Object[] {}).getClass().getComponentType());

		for (final Integer single : new Integer[] { Integer.MIN_VALUE, -111, -11, -1, 0, 1, 2, 3, 4, Integer.MAX_VALUE }) {
			final Integer[] array = new Integer[] { single };
			assertArrayEquals(array, LazyArray.mutable.reverse(array));
			assertSame(array, LazyArray.mutable.reverse(array));
			assertEquals(Integer.class, LazyArray.mutable.reverse(array).getClass().getComponentType());

		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void ensureAccessToDesc() {
		for (final int index : new int[] { Integer.MIN_VALUE, -1, 0, 1, 11, 23, Integer.MAX_VALUE })
			assertEquals("null[" + index + "]"//
					, LazyArray.accessToDesc(null, null, index));

		for (final Class componentType : new Class[] { Object.class, int.class, Void.class, Integer.class })
			for (final int index : new int[] { Integer.MIN_VALUE, -1, 0, 1, 11, 23, Integer.MAX_VALUE }) {
				assertEquals("null/" + LazyClass.ClassToString.ToAppropriateName.apply(componentType) + "<~>[" + index + "]"//
						, LazyArray.accessToDesc(null, componentType, index));
			}
		for (final Class componentType : new Class[] { new Object[] {}.getClass(), new int[] {}.getClass(), new Integer[] {}.getClass() })
			for (final int index : new int[] { Integer.MIN_VALUE, -1, 0, 1, 11, 23, Integer.MAX_VALUE }) {
				assertEquals("null/" + LazyClass.ClassToString.ToAppropriateName.apply(componentType.getComponentType()) + "<~>[" + index + "][]"//
						, LazyArray.accessToDesc(null, componentType, index));
			}

		for (final int index : new int[] { Integer.MIN_VALUE, -1, 0, 1, 11, 23, Integer.MAX_VALUE }) {
			assertEquals("Object<0~2>[" + index + "]" //
					, LazyArray.accessToDesc(new Object[] { 1, 2, 33 }, null, index));
			assertEquals("Object<~>[" + index + "]" //
					, LazyArray.accessToDesc(new Object[] {}, null, index));
			assertEquals("int<0~2>[" + index + "][]" //
					, LazyArray.accessToDesc(new int[][] { new int[] { 0 }, new int[] { 1 }, new int[] { 2 } }, null, index));
			assertEquals("int<~>[" + index + "][][]" //
					, LazyArray.accessToDesc(new int[][][] {}, null, index));

		}

	}

	@Test
	public void ensureCloneAll() {
		// int[] 
	}

	@Test
	public void ensureCloneAllForBoolean() {
		for (boolean[] array : new boolean[][] { null //
				, new boolean[] {}//
				, new boolean[] { true }//
				, new boolean[] { false }//
				, new boolean[] { nextBoolean() }//
				, new boolean[] { nextBoolean(), nextBoolean() } }) {
			final boolean[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone);
		}
		for (boolean[][][] array : new boolean[][][][] { //
				{}//
				, { { {} } }//
				, { { { nextBoolean() } } }//
				, { { { nextBoolean(), nextBoolean() } } }//
				, { { { nextBoolean(), nextBoolean() }, null, null } }//
				, { { { nextBoolean(), nextBoolean() }, { nextBoolean() } } }//
				, { { { nextBoolean(), nextBoolean() }, { nextBoolean() } }, { { nextBoolean(), nextBoolean() }, { nextBoolean() } } }//
				, { { { nextBoolean() } }, { { nextBoolean() } } }//
				, { { { nextBoolean() } } }//
		}) {
			/* clone */ {
				final boolean[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single vlue */ {
				final boolean[][][] change = LazyArray.cloneAll(array);
				for (boolean[][] arrayD2 : LazyNull.ifNull(change, new boolean[][][] {})) {
					for (boolean[] arrayD1 : LazyNull.ifNull(arrayD2, new boolean[][] {})) {
						for (int i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = !arrayD1[i];
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = !arrayD1[i];
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final boolean[][][] change = LazyArray.cloneAll(array);
				for (int i = 0; i < change.length; i++) {
					final boolean[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final boolean[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	@Test
	public void ensureCloneAllForByte() {
		for (byte[] array : new byte[][] { null //
				, new byte[] {}//
				, new byte[] { 0 }//
				, new byte[] { (byte) nextInt() }//
				, new byte[] { (byte) nextInt(), (byte) nextInt() } }) {
			final byte[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone);
		}
		for (byte[][][] array : new byte[][][][] { //
				{}//
				, { { {} } }//
				, { { { (byte) nextInt(3) } } }//
				, { { { (byte) nextInt(3), (byte) nextInt() } } }//
				, { { { (byte) nextInt(3), (byte) nextInt() }, null, null } }//
				, { { { (byte) nextInt(3), (byte) nextInt() }, { (byte) nextInt(3) } } }//
				, { { { (byte) nextInt(3), (byte) nextInt() }, { (byte) nextInt(3) } }, { { (byte) nextInt(3), (byte) nextInt() }, { (byte) nextInt(3) } } }//
				, { { { (byte) nextInt(3) } }, { { (byte) nextInt(3) } } }//
				, { { { (byte) nextInt(3) } } }//
		}) {
			/* clone */ {
				final byte[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single vlue */ {
				final byte[][][] change = LazyArray.cloneAll(array);
				for (byte[][] arrayD2 : LazyNull.ifNull(change, new byte[][][] {})) {
					for (byte[] arrayD1 : LazyNull.ifNull(arrayD2, new byte[][] {})) {
						for (byte i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = (byte) (0xFFFFFFFF ^ arrayD1[i]);
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = (byte) (0xFFFFFFFF ^ arrayD1[i]);
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final byte[][][] change = LazyArray.cloneAll(array);
				for (byte i = 0; i < change.length; i++) {
					final byte[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final byte[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	@Test
	public void ensureCloneAllForChar() {
		for (char[] array : new char[][] { null //
				, new char[] {}//
				, new char[] { 0 }//
				, new char[] { (char) nextInt() }//
				, new char[] { (char) nextInt(), (char) nextInt() } }) {
			final char[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone);
		}
		for (char[][][] array : new char[][][][] { //
				{}//
				, { { {} } }//
				, { { { (char) nextInt(3) } } }//
				, { { { (char) nextInt(3), (char) nextInt() } } }//
				, { { { (char) nextInt(3), (char) nextInt() }, null, null } }//
				, { { { (char) nextInt(3), (char) nextInt() }, { (char) nextInt(3) } } }//
				, { { { (char) nextInt(3), (char) nextInt() }, { (char) nextInt(3) } }, { { (char) nextInt(3), (char) nextInt() }, { (char) nextInt(3) } } }//
				, { { { (char) nextInt(3) } }, { { (char) nextInt(3) } } }//
				, { { { (char) nextInt(3) } } }//
		}) {
			/* clone */ {
				final char[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single vlue */ {
				final char[][][] change = LazyArray.cloneAll(array);
				for (char[][] arrayD2 : LazyNull.ifNull(change, new char[][][] {})) {
					for (char[] arrayD1 : LazyNull.ifNull(arrayD2, new char[][] {})) {
						for (char i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = (char) (0xFFFFFFFF ^ arrayD1[i]);
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = (char) (0xFFFFFFFF ^ arrayD1[i]);
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final char[][][] change = LazyArray.cloneAll(array);
				for (char i = 0; i < change.length; i++) {
					final char[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final char[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	@Test
	public void ensureCloneAllForDouble() {
		for (double[] array : new double[][] { null //
				, new double[] {}//
				, new double[] { 0 }//
				, new double[] { nextInt() }//
				, new double[] { nextInt(), nextInt() } }) {
			final double[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone, 0);
		}
		for (double[][][] array : new double[][][][] { //
				{}//
				, { { {} } }//
				, { { { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() } } }//
				, { { { nextInt(3), nextInt() }, null, null } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } }, { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3) } }, { { nextInt(3) } } }//
				, { { { nextInt(3) } } }//
		}) {
			/* clone */ {
				final double[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single vlue */ {
				final double[][][] change = LazyArray.cloneAll(array);
				for (double[][] arrayD2 : LazyNull.ifNull(change, new double[][][] {})) {
					for (double[] arrayD1 : LazyNull.ifNull(arrayD2, new double[][] {})) {
						for (int i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = -arrayD1[i];
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = -arrayD1[i];
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final double[][][] change = LazyArray.cloneAll(array);
				for (int i = 0; i < change.length; i++) {
					final double[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final double[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	@Test
	public void ensureCloneAllForFloat() {
		for (float[] array : new float[][] { null //
				, new float[] {}//
				, new float[] { 0 }//
				, new float[] { nextInt() }//
				, new float[] { nextInt(), nextInt() } }) {
			final float[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone, 0.0f);
		}
		for (float[][][] array : new float[][][][] { //
				{}//
				, { { {} } }//
				, { { { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() } } }//
				, { { { nextInt(3), nextInt() }, null, null } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } }, { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3) } }, { { nextInt(3) } } }//
				, { { { nextInt(3) } } }//
		}) {
			/* clone */ {
				final float[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single vlue */ {
				final float[][][] change = LazyArray.cloneAll(array);
				for (float[][] arrayD2 : LazyNull.ifNull(change, new float[][][] {})) {
					for (float[] arrayD1 : LazyNull.ifNull(arrayD2, new float[][] {})) {
						for (int i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = -arrayD1[i];
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = -arrayD1[i];
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final float[][][] change = LazyArray.cloneAll(array);
				for (int i = 0; i < change.length; i++) {
					final float[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final float[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	@Test
	public void ensureCloneAllForInt() {
		for (int[] array : new int[][] { null //
				, new int[] {}//
				, new int[] { 0 }//
				, new int[] { nextInt() }//
				, new int[] { nextInt(), nextInt() } }) {
			final int[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone);
		}
		for (int[][][] array : new int[][][][] { //
				{}//
				, { { {} } }//
				, { { { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() } } }//
				, { { { nextInt(3), nextInt() }, null, null } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } }, { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3) } }, { { nextInt(3) } } }//
				, { { { nextInt(3) } } }//
		}) {
			/* clone */ {
				final int[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single value */ {
				final int[][][] change = LazyArray.cloneAll(array);
				for (int[][] arrayD2 : LazyNull.ifNull(change, new int[][][] {})) {
					for (int[] arrayD1 : LazyNull.ifNull(arrayD2, new int[][] {})) {
						for (int i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = 0xFFFFFFFF ^ arrayD1[i];
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = 0xFFFFFFFF ^ arrayD1[i];
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final int[][][] change = LazyArray.cloneAll(array);
				for (int i = 0; i < change.length; i++) {
					final int[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final int[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	@Test
	public void ensureCloneAllForLong() {
		for (long[] array : new long[][] { null //
				, new long[] {}//
				, new long[] { 0 }//
				, new long[] { nextInt() }//
				, new long[] { nextInt(), nextInt() } }) {
			final long[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone);
		}
		for (long[][][] array : new long[][][][] { //
				{}//
				, { { {} } }//
				, { { { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() } } }//
				, { { { nextInt(3), nextInt() }, null, null } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3), nextInt() }, { nextInt(3) } }, { { nextInt(3), nextInt() }, { nextInt(3) } } }//
				, { { { nextInt(3) } }, { { nextInt(3) } } }//
				, { { { nextInt(3) } } }//
		}) {
			/* clone */ {
				final long[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single vlue */ {
				final long[][][] change = LazyArray.cloneAll(array);
				for (long[][] arrayD2 : LazyNull.ifNull(change, new long[][][] {})) {
					for (long[] arrayD1 : LazyNull.ifNull(arrayD2, new long[][] {})) {
						for (int i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = 0xFFFFFFFFFFFFFFFFL ^ arrayD1[i];
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = 0xFFFFFFFFFFFFFFFFL ^ arrayD1[i];
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final long[][][] change = LazyArray.cloneAll(array);
				for (int i = 0; i < change.length; i++) {
					final long[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final long[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	@Test
	public void ensureCloneAllForShort() {
		for (short[] array : new short[][] { null //
				, new short[] {}//
				, new short[] { 0 }//
				, new short[] { (short) nextInt() }//
				, new short[] { (short) nextInt(), (short) nextInt() } }) {
			final short[] clone = LazyArray.cloneAll(array);
			assertArrayEquals(array, clone);
		}
		for (short[][][] array : new short[][][][] { //
				{}//
				, { { {} } }//
				, { { { (short) nextInt(3) } } }//
				, { { { (short) nextInt(3), (short) nextInt() } } }//
				, { { { (short) nextInt(3), (short) nextInt() }, null, null } }//
				, { { { (short) nextInt(3), (short) nextInt() }, { (short) nextInt(3) } } }//
				,
				{ { { (short) nextInt(3), (short) nextInt() }, { (short) nextInt(3) } }, { { (short) nextInt(3), (short) nextInt() }, { (short) nextInt(3) } } }//
				, { { { (short) nextInt(3) } }, { { (short) nextInt(3) } } }//
				, { { { (short) nextInt(3) } } }//
		}) {
			/* clone */ {
				final short[][][] clone = LazyArray.cloneAll(array);
				assertArrayEquals(array, clone);
			}
			/* change single vlue */ {
				final short[][][] change = LazyArray.cloneAll(array);
				for (short[][] arrayD2 : LazyNull.ifNull(change, new short[][][] {})) {
					for (short[] arrayD1 : LazyNull.ifNull(arrayD2, new short[][] {})) {
						for (short i = 0; i < (arrayD1 != null ? arrayD1.length : -1); i++) {
							// change
							arrayD1[i] = (short) (0xFFFFFFFF ^ arrayD1[i]);
							assertArrayNotEquals(array, change);
							// restore
							arrayD1[i] = (short) (0xFFFFFFFF ^ arrayD1[i]);
							assertArrayEquals(array, change);
						}
					}
				}
			}
			/* change subarray */ {
				final short[][][] change = LazyArray.cloneAll(array);
				for (short i = 0; i < change.length; i++) {
					final short[][] arrayD2 = change[i];
					if (arrayD2 != null && arrayD2.length > 0) {
						final short[][] changeD2 = Arrays.copyOf(arrayD2, arrayD2.length - 1);
						change[i] = changeD2;
						assertArrayNotEquals(array, change);
						// restore
						change[i] = arrayD2;
						assertArrayEquals(array, change);
					}
				}
			}
		}
	}

	private final boolean nextBoolean() {
		return random.nextBoolean();
	}

	private final int nextInt() {
		return random.nextInt();
	}

	private final int nextInt(int bound) {
		return random.nextInt(bound);
	}

}
