/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.collections.primitives;

import java.util.Iterator;

import org.lazylibrary.lazy.LazyPrimitives;
import org.lazylibrary.lazy.LazyPrimitives.compaction;

/**
 * 0,1,2,...(n-1) elements are split into sub sets<br />
 * Elements are returned as array of first and last element (inclusive) of subsets in {@link compaction} form <br />
 * For <b>n = 3 </b>:<br/>
 * <br />
 * {{0;1}{2;2}}<br />
 * {{0;0}{1;1}{2;2}}<br />
 * {{0;0}{1;2}}<br />
 * {{0;2}}<br />
 * 
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class SetSplitterToContinuousIntSubsets implements Iterator<int[]>, Iterable<int[]> {
	public SetSplitterToContinuousIntSubsets(int size) {
		if (size > 32) { throw new IllegalArgumentException(this.getClass().getSimpleName() + " support up to 31 elements"); }
		this.subsets = (~0) >>> (32 - size + 1);
		this.size = size;
	}

	int subsets;
	int size;

	@Override
	public boolean hasNext() {
		return subsets >= 0;
	}

	public int left() {
		return subsets;
	}

	@Override
	public int[] next() {
		return subsetToSets(size, subsets--);
	}

	private static int[] subsetToSets(final int bytes, int subsets) {
		if (bytes == 0) { return new int[] {}; }
		final int[] ret = new int[countSubSets(bytes, subsets)];
		int retPos = 0;
		int pre = 0;
		for (int left = bytes - 1; left > 0; left--) {
			if (((subsets ^ (subsets >> 1)) & 1) == 1) {
				ret[retPos++] = LazyPrimitives.compaction.compact((short) pre, (short) (bytes - left - 1));
				if (false) System.out.println(pre + "~" + (bytes - left - 1));
				pre = bytes - left;
			}
			subsets >>>= 1;
		}
		ret[retPos++] = LazyPrimitives.compaction.compact((short) pre, (short) (bytes - 0 - 1));
		if (false) System.out.println(pre + "~" + (bytes - 1));
		Integer.bitCount(subsets ^ (subsets >>> 1));
		return ret;
	}

	private static int countSubSets(int bytes, int subsets) {
		if (bytes == 0) { return 0; }
		return Integer.bitCount((subsets ^ (subsets >>> 1)) & (0x7FFFFFFF >>> (32 - bytes))) + 1;
	}

	public static void main(String[] args) {
		final SetSplitterToContinuousIntSubsets setSpliter = new SetSplitterToContinuousIntSubsets(3);
		final int size = setSpliter.left();
		System.out.println(size);
		final long time = System.currentTimeMillis();
		for (int[] ss : setSpliter) {
			System.out.println(LazyPrimitives.compaction.toString(ss));
		}
		System.out.println((System.currentTimeMillis() - time));
		if (false) {

			System.out.println(LazyPrimitives.compaction.toString(subsetToSets(8, 0b11001100)));
			System.out.println(subsetToSets(8, 0b10));
			System.out.println(countSubSets(8 * 3, 0b110011001100110011001100));
			System.out.println(countSubSets(8, 0b11001100));
			System.out.println(countSubSets(8, 0b00110011));
			System.out.println(countSubSets(8, 0b11111100));
			System.out.println(countSubSets(8, 0b00000011));
			System.out.println(countSubSets(8, 0b11111111));
			System.out.println(countSubSets(8, ~0b11111111));
			System.out.println(countSubSets(8, 0b0));
			System.out.println(countSubSets(8, 0b11001100));

		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<int[]> iterator() {
		return this;
	}

}
