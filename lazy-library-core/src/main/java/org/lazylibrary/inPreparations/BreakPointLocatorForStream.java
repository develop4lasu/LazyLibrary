/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.inPreparations;

import java.io.IOException;
import java.util.function.Consumer;

import org.lazylibrary.lazy.LazyNull;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class BreakPointLocatorForStream extends java.io.OutputStream {

	private static class DataChunk implements Comparable<DataChunk> {
		public static final DataChunk[] emptyArray = new DataChunk[] {};
		protected Consumer breakPointConsumer;
		@IsSortedNaturally
		protected DataChunk[] followingChunks = emptyArray;
		protected byte[] fragment;

		@Override
		public int compareTo(DataChunk o) {
			LazyNull.ensure.noNull(o, DataChunk.class);
			return (this.fragment[0] & 0xFF) - (o.fragment[0] & 0xFF);
		}

		public int getLeadingValue() {
			return (this.fragment[0] & 0xFF);
		}
	}

	private DataChunk current;
	private int pos;

	private DataChunk root = null;

	public BreakPointLocatorForStream registerBreakPoint(byte[] streamPart, Consumer breakPointConsumer) {
		//FIXME
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see java.io.OutputStream#write(int)
	 */
	@Override
	public void write(int b) throws IOException {
		// TODO Auto-generated method stub

	}

}
