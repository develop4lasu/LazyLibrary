/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */
package org.lazylibrary.lazy;

import java.util.function.Supplier;

import org.lazylibrary.conventions.Root4Lazy;
import org.lazylibrary.traits.IsNull4NullRule;
import org.lazylibrary.conventions.IsEnsuringConvention;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class LazyNull implements Root4Lazy {

	/**
	 * Methods collected under ensure group throws exceptions in case of finding invalid state pointed by their name
	 */
	public static class ensure {

		@IsEnsuringConvention
		public final static <T> T noNull(T value, @SuppressWarnings("rawtypes") Class valueClass) {
			if (valueClass == null) { throw new NullPointerException("valueClass"); }
			if (value == null) { throw new NullPointerException(valueClass.getSimpleName()); }
			return value;
		}

		@IsEnsuringConvention
		public final static <T> T noNull(T value, String npeMessage) {
			if (npeMessage == null) { throw new NullPointerException("npeMessage"); }
			if (value == null) { throw new NullPointerException(npeMessage); }
			return value;
		}

		@IsEnsuringConvention
		public final static <T> T noNull(final T value, final Supplier<String> npeMessageSupplier) {
			if (npeMessageSupplier == null) { throw new NullPointerException("npeMessageSupplier"); }
			if (value == null) {
				final String npeMessage = npeMessageSupplier.get();
				if (npeMessage == null) { throw new NullPointerException(Supplier.class.getSimpleName() + " did not provided npeMessage"); }
				throw new NullPointerException(npeMessage);
			}
			return value;
		}

		/**
		 * @see LazyArray.ensure#noNulls(Object[])
		 */
		@IsEnsuringConvention
		@IsNull4NullRule
		final public static <T> T[] noNullsInArray(T[] array) {
			if (array == null || array.length == 0) {}
			else {
				for (int i = 0; i < array.length; i++) {
					if (array[i] == null) { //
						throw new NullPointerException(LazyClass.ClassToString.ToAppropriateName.apply(array.getClass()) + " at index " + i);
					}
				}
			}
			return array;
		}
	}

	public final static <T, Der extends T> T ifNull(T value, Supplier<? extends T> inExchangeOfNull) {
		if (value != null) { return value; }
		if (inExchangeOfNull == null) { throw new NullPointerException(Supplier.class.getSimpleName()); }
		return inExchangeOfNull.get();
	}

	public final static <T> T ifNull(T value, T inExchangeOfNull) {
		final T ret = value != null ? value : inExchangeOfNull;
		return ret;
	}

	public final static <T> T ifNulls(@SuppressWarnings("unchecked") T... values) {
		if (false /* sample code */) {
			java.lang.System.out.println(ifNull(nullOf(Integer.class), (() -> LazyNull.<Integer>ifNulls(null, null, 2))));
		}
		if (values == null) { return null; }
		for (final T value : values) {
			if (value != null) { return value; }
		}
		return null;
	}

	/**
	 * Allow to properly refer to value after it's reduction to null
	 * 
	 * @return <code>null</code>
	 */
	public final static <T> T nullOf(@SuppressWarnings("unused") Class<T> theClass) {
		return null;
	}

	/**
	 * Allow to properly refer to value after it's reduction to null
	 * 
	 * @return <code>null</code>
	 */
	public final static <T, Out> Out toNull(@SuppressWarnings("unused") T value) {
		return null;
	}

}
