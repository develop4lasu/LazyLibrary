/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import org.lazylibrary.conventions.Root4Lazy;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class LazyPrimitives implements Root4Lazy {

	public static class cast {

		public static byte toByte(int value) {
			if (value < Byte.MIN_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to low to be converted to " + Byte.class.getSimpleName());
			}
			if (value > Byte.MAX_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to high to be converted to " + Byte.class.getSimpleName());
			}
			return (byte) value;
		}

		public static byte toByte(long value) {
			if (value < Byte.MIN_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to low to be converted to " + Byte.class.getSimpleName());
			}
			if (value > Byte.MAX_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to high to be converted to " + Byte.class.getSimpleName());
			}
			return (byte) value;
		}

		public static byte toByte(short value) {
			if (value < Byte.MIN_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to low to be converted to " + Byte.class.getSimpleName());
			}
			if (value > Byte.MAX_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to high to be converted to " + Byte.class.getSimpleName());
			}
			return (byte) value;
		}

		public static int toInt(long value) {
			if (value < Integer.MIN_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to low to be converted to " + Integer.class.getSimpleName());
			}
			if (value > Integer.MAX_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to high to be converted to " + Integer.class.getSimpleName());
			}
			return (int) value;
		}

		public static short toShort(int value) {
			if (value < Short.MIN_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to low to be converted to " + Short.class.getSimpleName());
			}
			if (value > Short.MAX_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to high to be converted to " + Short.class.getSimpleName());
			}
			return (short) value;
		}

		public static short toShort(long value) {
			if (value < Short.MIN_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to low to be converted to " + Short.class.getSimpleName());
			}
			if (value > Short.MAX_VALUE) { //
				throw new IllegalArgumentException("Given value(" + value + ") is to high to be converted to " + Short.class.getSimpleName());
			}
			return (short) value;
		}

	}

	/**
	 * Allow execute fast transaction
	 * 
	 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
	 */
	public static class compaction {
		private static final int intHalfMask = 0xFFFF;
		private static final int intHalfSize = 16;
		private static final long longHalfMask = 0xFFFFFFFFL;
		private static final int longHalfSize = 32;
		private static final short shortHalfMask = 0xFF;
		private static final int shortHalfSize = 8;

		/**
		 * @return compacted given bytes-s
		 */
		public static short compact(byte lower, byte upper) {
			return (short) ((shortHalfMask & lower) | ((upper) << shortHalfSize));
		}

		/**
		 * @return compacted given ints-s
		 */
		public static long compact(int lower, int upper) {
			return (longHalfMask & lower) | (((long) upper) << longHalfSize);
		}

		/**
		 * @return compacted given shorts-s
		 */
		public static int compact(short lower, short upper) {
			return (intHalfMask & lower) | ((upper) << intHalfSize);
		}

		/**
		 * @return 'lower' value
		 */
		public static short getLower(int compact) {
			return (short) compact;
		}

		public static String toString(int compact) {
			return "{" + getLower(compact) + ";" + getUpper(compact) + "}";
		}

		public static String toString(short compact) {
			return "{" + getLower(compact) + ";" + getUpper(compact) + "}";
		}

		public static String toString(long compact) {
			return "{" + getLower(compact) + ";" + getUpper(compact) + "}";
		}

		public static String toString(long[] compacts) {
			if (compacts == null) { return null; }
			final StringBuilder ret = new StringBuilder(compacts.length * 5);
			ret.append('{');
			for (final long compact : compacts) {
				ret.append(toString(compact));
			}
			ret.append('}');
			return ret.toString();
		}

		public static String toString(int[] compacts) {
			if (compacts == null) { return null; }
			final StringBuilder ret = new StringBuilder(compacts.length * 5);
			ret.append('{');
			for (final int compact : compacts) {
				ret.append(toString(compact));
			}
			ret.append('}');
			return ret.toString();
		}

		public static String toString(short[] compacts) {
			if (compacts == null) { return null; }
			final StringBuilder ret = new StringBuilder(compacts.length * 5);
			ret.append('{');
			for (final short compact : compacts) {
				ret.append(toString(compact));
			}
			ret.append('}');
			return ret.toString();
		}

		/**
		 * @return 'lower' value
		 */
		public static int getLower(long compact) {
			return (int) compact;
		}

		/**
		 * @return 'lower' value
		 */
		public static byte getLower(short compact) {
			return (byte) compact;
		}

		/**
		 * @return 'upper' value
		 */
		public static short getUpper(int compact) {
			return (short) (compact >>> intHalfSize);
		}

		/**
		 * @return 'upper' value
		 */
		public static int getUpper(long compact) {
			return (int) (compact >>> longHalfSize);
		}

		/**
		 * @return 'upper' value
		 */
		public static byte getUpper(short compact) {
			return (byte) (compact >>> shortHalfSize);
		}

		/**
		 * @return new compacted shorts-s
		 */
		public static int putLower(int compact, short lower) {
			return ((compact & (intHalfMask << intHalfSize)) | (lower & intHalfMask));
		}

		/**
		 * @return new compacted int-s
		 */
		public static long putLower(long compact, int lower) {
			return ((compact & (longHalfMask << longHalfSize)) | (lower & longHalfMask));
		}

		/**
		 * @return new compacted int-s
		 */
		public static short putLower(short compact, byte lower) {
			return (short) ((compact & (shortHalfMask << shortHalfSize)) | (lower & shortHalfMask));
		}

		/**
		 * @return new compacted shorts-s
		 */
		public static int putUpper(int compact, short upper) {
			return ((upper) << intHalfSize) | (compact & intHalfMask);
		}

		/**
		 * @return new compacted int-s
		 */
		public static long putUpper(long compact, int upper) {
			return (((long) upper) << longHalfSize) | (compact & longHalfMask);
		}

		/**
		 * @return new compacted int-s
		 */
		public static short putUpper(short compact, byte upper) {
			return (short) ((upper << shortHalfSize) | (compact & shortHalfMask));
		}

	}

}
