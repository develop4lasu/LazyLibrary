/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import java.lang.reflect.Array;
import java.util.Arrays;

import org.lazylibrary.conventions.IsEnsuringConvention;
import org.lazylibrary.conventions.Root4Lazy;
import org.lazylibrary.conventions.parameters.IsIndexExclusive;
import org.lazylibrary.conventions.parameters.IsIndexInclusive;
import org.lazylibrary.traits.IsNull4NullRule;

/**
 * <p>
 * <b>Subspaces</b>:<br/>
 * <b>secure</b>(default) all input and output parameters are secured<br />
 * <b>mutable</b> parameters under this space can be modified or passed as return parameter<br />
 * <b>primitives</b> space for primitives (due to amount of same methods)<br />
 * </p>
 * 
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class LazyArray implements Root4Lazy {

	public static final String inclusiveLeftBracket = "<";
	public static final String inclusiveRightBracket = ">";
	public static final String exclusiveLeftBracket = "(";
	public static final String exclusiveRightBracket = ")";

	/**
	 * @return new array containing all elements from first and second array
	 */
	public static <T /* , E extends T = no need */> T[] concat(T[] array, T[] second) {
		if (second == null) { return clone(array); }
		if (array == null) { throw new NullPointerException("array"); }
		T[] ret = Arrays.copyOf(array, array.length + second.length);
		System.arraycopy(second, 0, ret, array.length, second.length);
		return ret;
	}

	public static class ensure {

		final public static <T> T[] empty(T[] array) {
			if (array == null) {}
			else if (array.length == 0) {}
			else {
				throw new IllegalArgumentException();
			}
			return array;
		}

		/**
		 * @see LazyNull.ensure#noNullsInArray(Object[])
		 */
		@IsEnsuringConvention
		@IsNull4NullRule
		final public static <T> T[] noNulls(T[] array) {
			if (array == null || array.length == 0) {}
			else {
				for (int i = 0; i < array.length; i++) {
					if (array[i] == null) { throw new NullPointerException(
							LazyClass.ClassToString.ToAppropriateName.apply(array.getClass()) + " at index " + i); }
				}
			}
			return array;
		}

		private final static <T> void range(T[] array, @IsIndexInclusive int from, @IsIndexExclusive int to) {
			if (array != null) {
				final int length = array.length;
				if (from > to) { throw new IllegalArgumentException(accessToDesc(array, null, from + "~" + to)); }
				if (from < 0) { throw new ArrayIndexOutOfBoundsException(accessToDesc(array, null, from)); }
				if (to > length) { throw new ArrayIndexOutOfBoundsException(accessToDesc(array, null, to)); }

			}
		}

		final public static <T> T[] withElement(T[] array) {
			if (array == null) {
				throw new NullPointerException();
			}
			else if (array.length == 0) { throw new IllegalArgumentException(); }
			return array;
		}

	}

	/**
	 * Parameters under this space can be modified or passed as return parameter.
	 */
	public static class mutable {

		public static <T /* , E extends T = no need */> T[] concat(T[] array, T[] second) {
			if (second == null || second.length == 0) { return array; }
			if (array == null) { throw new NullPointerException("array"); }
			if (array.getClass().equals(second.getClass())) { return second; }
			T[] ret = Arrays.copyOf(array, array.length + second.length);
			System.arraycopy(second, 0, ret, array.length, second.length);
			return ret;
		}

		public static <T /* , E extends T = no need */> T[] concatAll(T[] array, T[]... arrays) {
			if (arrays == null || arrays.length == 0) { return array; }
			int len = 0;
			for (T[] ts : arrays) {
				len += (ts != null ? ts.length : 0);
			}
			int done;
			T[] ret = Arrays.copyOf(array, (done = array.length) + len);
			for (T[] t : arrays) {
				if (t != null) {
					System.arraycopy(t, 0, ret, done, t.length);
					done += t.length;
				}
			}
			return ret;
		}

		public static <Type> Type[] reverse(Type[] array) {
			if (array == null || array.length < 1) { return array; }
			int from = 0;
			int to = array.length - 1;
			while (from < to) {
				Type temp = array[from];
				array[from] = array[to];
				array[to] = temp;
				from++;
				to--;
			}
			return array;
		}

		/**
		 * Creates new array filled with elements from given array in reversed order
		 * <p />
		 * {@literal new[n] == array[array.length-n-1]}
		 * 
		 * @param array
		 *            the given array with reversed requested part, can be <code>null</code>
		 * @param from
		 *            the index of the first element (inclusive) for reverse
		 * @param to
		 *            the index of the last element (exclusive) for reverse
		 * @return back given array with reversed elements in given range, <code>null</code> if <code>null</code> was
		 *         passed
		 */
		public static <Type> Type[] reverse(Type[] array, @IsIndexInclusive int from, @IsIndexExclusive int to) {
			if (array == null) {
				return array;
			}
			else {
				ensure.range(array, from, to);
				final int length = array.length;
				if (length <= 1) {
					return array;
				}
				else {
					to--;
					while (from < to) {
						Type temp = array[from];
						array[from] = array[to];
						array[to] = temp;
						from++;
						to--;
					}
				}
			}
			return array;
		}

	}

	/**
	 * Space for primitives (due to amount of same methods)
	 */
	public static class primitives {

		@IsNull4NullRule
		final public static boolean[] clone(boolean[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

		@IsNull4NullRule
		final public static byte[] clone(byte[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

		@IsNull4NullRule
		final public static char[] clone(char[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

		@IsNull4NullRule
		final public static double[] clone(double[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

		@IsNull4NullRule
		final public static float[] clone(float[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

		@IsNull4NullRule
		final public static int[] clone(int[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

		@IsNull4NullRule
		final public static long[] clone(long[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

		@IsNull4NullRule
		final public static short[] clone(short[] array) {
			if (array == null || array.length == 0) { return array; }
			return array.clone();
		}

	}

	/**
	 * @return description of /failed/ access to array
	 */
	public final static <T> String accessToDesc(T[] array, Class<? super T> theComponentClass, int index) {
		return accessToDesc(array, theComponentClass, Integer.toString(index));
	}

	/**
	 * @return description of /failed/ access to array
	 */
	public final static <T> String accessToDesc(T[] array, Class<? super T> theComponentClass, String index) {
		if (array == null && theComponentClass == null) {
			return "null[" + index + "]";
		}
		else {
			final String theClass = array != null ? LazyClass.ClassToString.ToAppropriateName.apply(array.getClass())//
					: "null/" + LazyClass.ClassToString.ToAppropriateName.apply(theComponentClass) + "[]";
			final int bracket = theClass.indexOf('[');
			final int length = array != null ? array.length : 0;
			if (length == 0) {
				return theClass.substring(0, bracket) + inclusiveLeftBracket + "~" + inclusiveRightBracket + "[" + index + theClass.substring(bracket + 1);
			}
			else {
				return theClass.substring(0, bracket) + inclusiveLeftBracket + "0~" + (length - 1) + inclusiveRightBracket + "[" + index
						+ theClass.substring(bracket + 1);
			}
		}
	}

	final public static String clasicRangeToDesc(@IsIndexInclusive int from, @IsIndexExclusive int to) {
		return inclusiveLeftBracket + from + "~" + to + exclusiveRightBracket;
	}

	@IsNull4NullRule
	final public static <T> T[] clone(T[] array) {
		if (array == null || array.length == 0) { return array; }
		return array.clone();
	}

	@SuppressWarnings("unchecked")
	@IsNull4NullRule
	final public static <T> T cloneAll(T object) {
		if (object == null) { return object; }
		final Class<? extends Object> theClass = object.getClass();
		if (theClass.isArray()) {
			final Class<?> componentType = theClass.getComponentType();
			if (componentType.isPrimitive()) {
				switch (componentType.getName()) {
				case "boolean":
					return (T) ((boolean[]) object).clone();
				case "byte":
					return (T) ((byte[]) object).clone();
				case "char":
					return (T) ((char[]) object).clone();
				case "double":
					return (T) ((double[]) object).clone();
				case "float":
					return (T) ((float[]) object).clone();
				case "int":
					return (T) ((int[]) object).clone();
				case "long":
					return (T) ((long[]) object).clone();
				case "short":
					return (T) ((short[]) object).clone();
				default:
					throw new RuntimeException("Unimplemented primitive: " + componentType.getName());
				}
			}
			else {
				final Object[] array = (Object[]) object;
				if (array.length == 0) { return object; }
				Object[] ret = array.clone();
				if (componentType.isArray() || componentType.getName().equals(Object.class.getName())) {
					for (int i = 0; i < ret.length; i++) {
						ret[i] = cloneAll(ret[i]);
					}
					return (T) ret.clone();
				}
				else {
					return (T) array.clone();
				}
			}
		}
		else {
			return object;
		}
	}

	/**
	 * @see #indexOfSame(Object[], Object)
	 */
	public static <T> boolean containsSame(T[] array, T elementToFind) {
		if (array != null) for (int i = 0; i < array.length; i++) {
			T t = array[i];
			if (t == elementToFind) { return true; }
		}
		return false;
	}

	/**
	 * @see LazyClass#createArray(Class, int)
	 */
	@SuppressWarnings("unchecked")
	@IsNull4NullRule
	final public static <T> T[] create(Class<? super T> cls, int length) {
		if (cls == null) { return null; }
		return (T[]) java.lang.reflect.Array.newInstance(cls, length);
	}

	/**
	 * @see LazyClass#createArray(Class)
	 */
	@SuppressWarnings("unchecked")
	@IsNull4NullRule
	final public static <T> T[] create(Class<T> cls) {
		if (cls == null) { return null; }
		return (T[]) java.lang.reflect.Array.newInstance(cls, 0);
	}

	/*
	 * Subspaces:
	 * secure(default) all in and out are secured
	 * economic can reuse() input parameters to decrease
	 */

	@IsNull4NullRule
	final public static Integer getLength(Object array) {
		if (array == null) { return null; }
		return Array.getLength(array);
	}

	/**
	 * @see #containsSame(Object[], Object)
	 */
	public static <T> int indexOfSame(T[] array, T elementToFind) {
		if (array != null) for (int i = 0; i < array.length; i++) {
			T t = array[i];
			if (t == elementToFind) { return i; }
		}
		return -1;
	}

	@IsNull4NullRule
	public final static Boolean isArray(Object object) {
		if (object == null) { return null; }
		return object.getClass().isArray();
	}

	/**
	 * @see LazyClass#isArray(Class)
	 */
	@IsNull4NullRule
	public static Boolean isClassArrayType(Class<?> theClass) {
		if (theClass == null) { return null; }
		return theClass.getComponentType() != null;
	}

	public static void main(String[] args) {
		System.out.println(accessToDesc(new int[][] {}, null, 2));
	}

	public static <T> T[] reduceToEmpty(T[] array) {
		if (array == null) { return array; }
		return Arrays.copyOf(array, /* newLength */ 0);
	}

	/**
	 * @return <i>insertion point</i> from <tt>(-(<i>insertion point</i>) - 1)</tt> returned by
	 *         {@link Arrays#binarySearch(Object[], Object)}
	 * @see Arrays#binarySearch(Object[], Object)
	 */
	public final static int regainInsertionPoint(int index) {
		if (index >= 0) { return index; }
		return -(index + 1);
	}

	public static <Type> Type[] reversed(final Type[] array) {
		if (array == null || array.length < 1) { return array; }
		final Type[] ret = Arrays.copyOf(array, array.length);
		int from = 0;
		int to = ret.length - 1;
		while (from < to) {
			Type temp = ret[from];
			ret[from] = ret[to];
			ret[to] = temp;
			from++;
			to--;
		}
		return ret;
	}

}
