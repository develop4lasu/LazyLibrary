/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import java.util.Random;

import org.lazylibrary.conventions.Root4Lazy;
import org.lazylibrary.conventions.parameters.IsIndexExclusive;
import org.lazylibrary.conventions.parameters.IsIndexInclusive;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class LazyRandom implements Root4Lazy {

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	private static Random random = new Random(System.currentTimeMillis());

	public static int[] intsInRange(@IsIndexInclusive int from, @IsIndexExclusive int to, int baseAmmount) {
		if (baseAmmount < 0) { throw new IllegalArgumentException("Negative baseAmmount(" + baseAmmount + ")"); }
		long space = ((long) to) - from;
		if (space >= Integer.MAX_VALUE) { throw new IllegalArgumentException("Space for (" + from + "," + to + "> baseAmmount(" + baseAmmount + ")"); }
		final int range = LazyPrimitives.cast.toInt(space);
		final int ammount = baseAmmount;
		final int[] ret = new int[ammount];
		int pos = 0;
		while (pos < ammount) {
			ret[pos++] = random.nextInt(range) + from;
		}
		return ret;
	}

	public static class append {

		public append() {
		}

		public static int[] intsInRange(int[] direct, //
				@IsIndexInclusive int from, @IsIndexExclusive int to, int baseAmmount) {
			if (baseAmmount < 0) { throw new IllegalArgumentException("Negative baseAmmount(" + baseAmmount + ")"); }
			long space = ((long) to) - from;
			if (space >= Integer.MAX_VALUE) { throw new IllegalArgumentException("Space for (" + from + "," + to + "> baseAmmount(" + baseAmmount + ")"); }
			final int range = LazyPrimitives.cast.toInt(space);
			final int ammount = baseAmmount + (direct != null ? direct.length : 0);
			final int[] ret = new int[ammount];
			int pos = 0;
			if (direct != null && direct.length > 0) {
				System.arraycopy(direct, 0, ret, 0, pos = direct.length);
			}
			while (pos < ammount) {
				ret[pos++] = random.nextInt(range) + from;
			}
			return ret;
		}

	}

	//
	//	public static int[] representativeOfRange(@IsIndexInclusive int from, @IsIndexExclusive int to, int ammount,int ...numbersToInclude) {
	//		if (ammount<0){
	//			throw new IllegalArgumentException();//TODO
	//		}
	//		long space = ((long)to)-from;
	//		if (space<){
	//			
	//		}else if (space<Integer.MAX_VALUE){
	//			
	//		}
	//		else{
	//			
	//		}
	//		
	//	}

}
