/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import java.util.function.Function;

import org.lazylibrary.conventions.Root4Lazy;
import org.lazylibrary.traits.IsNull4NullRule;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class LazyClass implements Root4Lazy {

	public enum ClassToString implements Function<Class<?>, String> {
		ToAppropriateName {
			@Override
			public String apply(Class<?> t) {
				if (t == null) { return null; }
				if (t.isPrimitive()) { return t.getCanonicalName(); }

				Class<?> enclosingClass = t.getEnclosingClass();
				if (enclosingClass == null) {
					enclosingClass = t;
				}
				Package enclosingPackage = enclosingClass.getPackage();
				if (enclosingPackage == null) {
					enclosingPackage = t.getPackage();
				}
				if (enclosingPackage == null) {
					enclosingPackage = getPackage(getBaseComponentType(t));
				}

				String name = t.getCanonicalName();
				if (name == null) {
					name = t.getName();
				}
				String ret = (enclosingPackage != null && name != null) ? //
				LazyString.removePrefix(enclosingPackage.getName() + ".", name)//
						: name;
				if (t.isAnonymousClass()) {
					Class<?>[] interfaces = t.getInterfaces();
					if (interfaces.length > 0) {
						return ret + " " + interfaces[0].getSimpleName() + "()";
					}
					else {
						return ret;
					}
				}
				else {// `
					return ret;
				}
			}
		}, //
		ToCannonicalName {
			@Override
			public String apply(Class<?> t) {
				if (t == null) { return null; }
				return t.getCanonicalName();
			}
		}, // ,//
		ToName {
			@Override
			public String apply(Class<?> t) {
				if (t == null) { return null; }
				return t.getName();
			}
		}, // ,//
		ToSimpleName {
			@Override
			public String apply(Class<?> t) {
				if (t == null) { return null; }
				return t.getSimpleName();
			}
		},//
		;

		/*
		 * (non-Javadoc)
		 * @see java.util.function.Function#apply(java.lang.Object)
		 */
		@Override
		public abstract String apply(Class<?> t);
	}

	@IsNull4NullRule
	public static Boolean canBeArray(Class<?> theClass) {
		if (theClass == null) { return null; }
		final Class<?> componentType = theClass.getComponentType();
		//if (componentType.equals(Object.class)) { return true; }
		return componentType != null;
	}

	/**
	 * @see LazyArray#create(Class, int)
	 */
	@SuppressWarnings("unchecked")
	@IsNull4NullRule
	final public static <T> T[] createArray(Class<? super T> cls, int length) {
		if (cls == null) { return null; }
		return (T[]) java.lang.reflect.Array.newInstance(cls, length);
	}

	/**
	 * @see LazyArray#create(Class)
	 */
	@SuppressWarnings("unchecked")
	@IsNull4NullRule
	final public static <T> T[] createArray(Class<T> cls) {
		if (cls == null) { return null; }
		return (T[]) java.lang.reflect.Array.newInstance(cls, 0);
	}

	@IsNull4NullRule
	final public static Class<?> getBaseComponentType(Class<?> theClass) {
		if (theClass == null) { return null; }
		while (theClass.isArray()) {
			theClass = theClass.getComponentType();
		}
		return theClass;
	}

	@IsNull4NullRule
	final public static Package getPackage(Class<?> theClass) {
		if (theClass == null) { return null; }
		return theClass.getPackage();
	}

	@IsNull4NullRule
	public static Boolean isArray(Class<?> theClass) {
		if (theClass == null) { return null; }
		return theClass.getComponentType() != null;
	}

}
