/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class LazyString {

	/**
	 * Given {@link String} will have <b>once</b> removed prefix and will be
	 * returned. This mean that returned {@link String} can still start with
	 * removed prefix, like: <code>removePrefixIfExists("aaa","a")</code> will
	 * effect with returning <code>"aa"</code>
	 * 
	 * @param string
	 *            {@link String} to proceed
	 * @param expectedPrefix
	 *            prefix to remove
	 * @return {@link String} with removed prefix
	 * @throws NullPointerException
	 *             if any parameter is <code>null</code>
	 * @throws IllegalArgumentException
	 *             if given {@link String} do not start with expected prefix
	 */
	final public static String removePrefix(String expectedPrefix, String string) {
		// Validate
		if (string == null) { throw new NullPointerException(String.class.getSimpleName()); }
		if (expectedPrefix == null) { throw new NullPointerException("prefix"); }
		if (string.startsWith(expectedPrefix)) {
			return string.substring(expectedPrefix.length());
		}
		else {
			throw new IllegalArgumentException(
					"Given " + String.class.getSimpleName() + "(" + string + ") do not start with expected prefix(" + expectedPrefix + ")");
		}
	}

	/**
	 * Given {@link String} will have <b>once</b> removed prefix and will be
	 * returned. This mean that returned {@link String} can still start with
	 * removed prefix, like: <code>removePrefixIfExists("aaa","a")</code> will
	 * effect with returning <code>"aa"</code>
	 * 
	 * @param string
	 *            {@link String} to proceed
	 * @param prefix
	 *            prefix to remove
	 * @return {@link String} with removed matched prefix
	 * @throws NullPointerException
	 *             if prefix is <code>null</code>
	 */
	// TODO: @ReturnNull4Null
	final public static String removePrefixIfExists(String string, String prefix) {
		if (prefix == null || prefix.isEmpty()) { return string; }
		if (string == null) { return null; }
		if (string.length() == 0) { return string; }
		if (string.startsWith(prefix)) { return string.substring(prefix.length()); }
		return string;
	}

}
