/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.lazy;

import java.util.function.Predicate;
import org.lazylibrary.conventions.Root4Lazy;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public class LazyPredicate implements Root4Lazy {

	@SuppressWarnings("rawtypes")
	public enum BasePredicate implements Predicate {
		AcceptAllPredicate {
			@Override
			public Predicate negate() {
				return BasePredicate.RejectAllPredicate;
			}

			@Override
			public boolean test(Object t) {
				return true;
			}
		}, //
		IsNotNullPredicate {
			@Override
			public Predicate negate() {
				return BasePredicate.IsNullPredicate;
			}

			@Override
			public boolean test(Object t) {
				return t != null;
			}
		}, //
		IsNullPredicate {
			@Override
			public Predicate negate() {
				return BasePredicate.IsNotNullPredicate;
			}

			@Override
			public boolean test(Object t) {
				return t == null;
			}
		}, //
		RejectAllPredicate {
			@Override
			public Predicate negate() {
				return BasePredicate.AcceptAllPredicate;
			}

			@Override
			public boolean test(Object t) {
				return false;
			}
		}, //
		;

		@SuppressWarnings("unchecked")
		public <Examined> Predicate<Examined> instance() {
			return this;
		}

		@Override
		public abstract Predicate negate();

		@Override
		public abstract boolean test(Object t);

	}

	public final static <T> T getFirst(Predicate<? super T> predicate, Iterable<T> iterable) {
		if (iterable == null) {
			return LazyNull.toNull(iterable);
		}
		for (T t : iterable) {
			if (predicate.test(t)) {
				return t;
			}
		}
		return LazyNull.toNull(iterable);
	}

	public final static <T> T getFirst(Predicate<? super T> predicate, T[] array) {
		if (array == null || array.length == 0) {
			return LazyNull.toNull(array);
		}
		for (T t : array) {
			if (predicate.test(t)) {
				return t;
			}
		}
		return LazyNull.toNull(array);
	}

}
