/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary;

import org.lazylibrary.conventions.IsEnsuringConvention;
import org.lazylibrary.traits.IsNull4NullRule;

/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */
public enum LazyLibraryQuestions {
	/**
	 * <b>New<b/><br />
	 * + shorter<br />
	 * - suggest that object may not be initialized<br />
	 * <b>Create<b/><br />
	 * - longer<br />
	 * 
	 * @see org.lazylibrary.lazy.LazyArray#create(Class, int)
	 */
	NewVsCreateConvention, //
	/**
	 * <b>Rule<b/><br />
	 * apply to data<br />
	 * <b>Convention<b/><br />
	 * apply to visible traits<br />
	 * 
	 * @see IsNull4NullRule
	 * @see IsEnsuringConvention
	 */
	RuleVsConvention, //
	/**
	 * <b>inclsive,from,to,inclusive , negative, positive, absolute </b><br />
	 * <b>[from,to)</b>
	 */
	ArrayRangeAccess, //
	/**
	 * T vs Type
	 */
	ComponentTypeLiteral, //
	/**
	 * < character is used instead of [ in order to prevent problems with array access notation.
	 */
	BracketNotations, //
	/**
	 * Current iterator visibility: < 1<br />
	 * Optimal iterator visibility: 2+<br />
	 * crossover iterators vs detached iterators vs partially detached iterators
	 * Problems with higher forward visibility: higher delay between input and output<br />
	 * Problems with higher forward visibility: GC problems with heavy data<br />
	 * Remove(direct) complication:
	 * Remove(indirect) complication:
	 * Optimistic/pessimistic access
	 */
	Iterator /* apply to collections as well */,// 
}
