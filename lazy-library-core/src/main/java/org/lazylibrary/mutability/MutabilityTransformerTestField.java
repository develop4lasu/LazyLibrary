/*
 * Copyright (c) 2017 Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of the above
 * copyright holders shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software
 * without prior written authorization.
 */

package org.lazylibrary.mutability;


/**
 * @author Marek Kozieł aka Lasu <develop4lasu[at]gmail.com>
 */

/***
 * Lockable ?
 * Based on Lazy
 * Based on Transformer(Mutable/immutable)
 * Zmienny / Builder / Mutable
 * NieZmienny / Staly / Immutable
 * Zmiennyna żądanie / Płynny / Switchable (support null - create ), composite / flow / zipper
 * reqestInterface()
 * requestMuttable() ask4
 * requestImmuttable()
 * immutable -> all immutable
 * BuilderCandidat - > immutable at first
 * mutable -> mutable
 * toMutable
 * toImmutable
 * ask4Mutable
 */

public class MutabilityTransformerTestField {

	// @formatter:off
	public interface S{
		S /* I */ getI();
		S /* M */ toM();
	}
	public interface PS{
		I getI();
		M toM();
	}
	public interface GS<M extends GS,I extends GS>{
		S /* I */ getI();
		S /* M */ toM();
	}
	public class M implements S{
		public M (){}
		public M (S s){}
		private I iCache /* optional */ = null;
		@Override public I getI() {return iCache!=null? iCache: new I(this);/* getFom(Static/Threat/Inherited)CacheMap<WeekRef> */}
		@Override public M toM() {return new M(this);}
	}
	public class I implements S{
		public I (S s){}
		@Override public I getI() {return this;}
		@Override public M toM() {return new M(this);}
	}
	public class P{}



}
